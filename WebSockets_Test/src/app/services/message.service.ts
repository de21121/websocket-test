import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { webSocket, WebSocketSubject } from 'rxjs/webSocket';
import { v4 as uuidv4 } from 'uuid';

@Injectable({
  providedIn: 'root'
})
export class MessageService {
  API_PATH_DEV: string = 'wss://localhost:44381/msg'
  API_PATH_STAGE: string = 'wss://87.228.12.67:80/msg'

  messageStream$: WebSocketSubject<any>;
  userName: string;

  constructor() {
  }

  public connect(userName?: string): void {

    var connectionId = uuidv4().toString();
    console.log(connectionId)
    if (userName && this.messageStream$) {
      this.messageStream$.complete();
    }

    var connectionUrl = `${this.API_PATH_STAGE}?connectionId=${connectionId}` + (userName ? `&user=${userName}` : '');
    this.messageStream$ = webSocket(connectionUrl);
    this.messageStream$.subscribe();
    this.userName = userName;

    window.addEventListener("beforeunload", (() => {
      this.messageStream$?.complete();
    }).bind(this));
  }

  public isCurrentUser(user: string) {
    return this.userName == user;
  }

  public sendMessage(message: string): void {
    this.messageStream$.next(message);
  }
}
