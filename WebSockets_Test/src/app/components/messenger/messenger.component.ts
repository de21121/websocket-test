import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { map, Subscription } from 'rxjs';
import { Message } from '../../models/message.model';
import { MessageService } from '../../services/message.service';
import { MessageComponent } from '../message/message.component';

@Component({
  selector: 'app-messenger',
  standalone: true,
  imports: [FormsModule, MessageComponent],
  templateUrl: './messenger.component.html',
  styleUrl: './messenger.component.scss'
})
export class MessengerComponent implements OnInit, OnDestroy {

  messages: Message[];
  isLoggedIn: boolean = false;
  userName: string;
  messageBody: string;
  messagesSubscription: Subscription;

  constructor(private webSocketService: MessageService) {

  }

  ngOnInit(): void {
    this.messages = [];
    this.webSocketService.connect();
    this.subscribeToConnection();
  }

  ngOnDestroy(): void {
    this.messagesSubscription?.unsubscribe();
  }

  login(): void {
    this.isLoggedIn = this.userName.length > 0;

    if (this.isLoggedIn) {
      this.webSocketService.connect(this.userName);
      this.subscribeToConnection();
    }
  }

  sendMessage(): void {
    this.webSocketService.sendMessage(this.messageBody);
    this.messageBody = '';
  }

  subscribeToConnection() {
    this.messagesSubscription?.unsubscribe();
      this.messagesSubscription = this.webSocketService.messageStream$.pipe(
        // map(
        //   message => {
        //     message.body = JSON.parse(message.body);
        //     return message
        //   }
        // )
      ).subscribe(message => {
        // message.body = JSON.parse(message.body)
        this.messages.push(message)
        this.messages.sort((a, b) => a.time.localeCompare(b.body));
      });
  }
}
