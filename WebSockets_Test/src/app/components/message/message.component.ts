import { DatePipe } from '@angular/common';
import { Component, Input, OnInit } from '@angular/core';
import { Message } from '../../models/message.model';
import { MessageService } from '../../services/message.service';

@Component({
  selector: 'app-message',
  standalone: true,
  imports: [DatePipe],
  templateUrl: './message.component.html',
  styleUrl: './message.component.scss'
})
export class MessageComponent implements OnInit {

  @Input() message: Message;
  JOIN_MESSAGE: string = '!join';
  EXIT_MESSAGE: string = '!leave';

  constructor(private messageService: MessageService) {
    
  }

  ngOnInit(): void {
    
  }

  isNotification(): boolean {
    return this.message.body.includes(this.JOIN_MESSAGE) || this.message.body.includes(this.EXIT_MESSAGE);
  }

  isCurrentUser(): boolean {
    return this.messageService.isCurrentUser(this.message.user)
  }

  trimQuotes(): string {
    let result = this.message.body;

    if (result.charAt(0) === "\"") {
        result = result.substring(1);
    }

    while (result.charAt(result.length - 1) === "\"") {
        result = result.substring(0, result.length - 1);
    }
    return result;
  }
}
