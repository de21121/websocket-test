import { Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { MessengerComponent } from './components/messenger/messenger.component';

export const routes: Routes = [
    {
        path: '*',
        component: AppComponent
    },
];
