import { Component } from '@angular/core';
import { RouterOutlet } from '@angular/router';
import { MessengerComponent } from './components/messenger/messenger.component';

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [RouterOutlet, MessengerComponent],
  templateUrl: './app.component.html',
  styleUrl: './app.component.scss'
})
export class AppComponent {
  title = 'WebSockets_Test';
}
