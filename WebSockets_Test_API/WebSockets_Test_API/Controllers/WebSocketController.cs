﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebSockets_Test_API.Services.Interfaces;

namespace WebSockets_Test_API.Controllers
{
    public class WebSocketController : ControllerBase
    {
        private readonly IWebSocketService _webSocketService;

        public WebSocketController(IWebSocketService webSocketService)
        {
            _webSocketService = webSocketService;
        }


        [Route("/msg")]
        public async Task GetMessage()
        {
            if (HttpContext.WebSockets.IsWebSocketRequest)
            {
                using var webSocket = await HttpContext.WebSockets.AcceptWebSocketAsync();

                string userName = null;
                if (HttpContext.Request.QueryString.HasValue && HttpContext.Request.QueryString.Value.Contains("user"))
                {
                    userName = HttpContext.Request.Query["user"];
                }

                Guid connectionId = Guid.Empty;
                Guid.TryParse(HttpContext.Request.Query["connectionId"], out connectionId);

                if (webSocket != null)
                {
                    await _webSocketService.RecieveMessage(webSocket, connectionId, userName);
                }
            }
            else
            {
                HttpContext.Response.StatusCode = StatusCodes.Status400BadRequest;
            }
        }
    }
}
