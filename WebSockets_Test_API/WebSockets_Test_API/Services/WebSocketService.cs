﻿using System.Net.WebSockets;
using System.Text;
using System.Text.Json;
using WebSockets_Test_API.Entities;
using WebSockets_Test_API.Services.Interfaces;

namespace WebSockets_Test_API.Services
{
    public class WebSocketService : IWebSocketService
    {
        private Dictionary<Guid, WebSocket> _connections;
        private readonly JsonSerializerOptions _options;

        public WebSocketService()
        {
            _connections = new Dictionary<Guid, WebSocket>();

            _options = new JsonSerializerOptions
            {
                PropertyNameCaseInsensitive = true,
                PropertyNamingPolicy = JsonNamingPolicy.CamelCase,
            };
        }

        public async Task RecieveMessage(WebSocket socket, Guid connectionId, string? userName = null)
        {
            if (!_connections.ContainsKey(connectionId)) {
                _connections.Add(connectionId, socket);
                
                if (userName != null)
                {
                    await SendEveryone("!join", userName);
                }
            }

            var buffer = new byte[1024 * 4];
            while (socket.State == WebSocketState.Open)
            {
                var result = await socket.ReceiveAsync(new ArraySegment<byte>(buffer), CancellationToken.None);
                var message = Encoding.UTF8.GetString(buffer, 0, result.Count);

                if (result.MessageType == WebSocketMessageType.Text)
                {
                    await SendEveryone(message, userName);
                } else if (result.MessageType == WebSocketMessageType.Close || socket.State == WebSocketState.Aborted)
                {
                    _connections.Remove(connectionId);

                    if (userName != null)
                    {
                        await SendEveryone("!leave", userName);
                    }

                    await socket.CloseAsync(result.CloseStatus.Value, result.CloseStatusDescription, CancellationToken.None);
                }
            }
        }

        private string ZipMessage(string messageBody, string sender)
        {
            var message = new Message()
            {
                Id = Guid.NewGuid(),
                User = sender,
                Body = messageBody,
                Time = DateTime.Now
            };

            //TODO: write messages to DB

            return JsonSerializer.Serialize(message, _options);
        }

        private async Task SendEveryone(string message, string sender)
        {
            var messageRaw = ZipMessage(message, sender);

            var buffer = Encoding.UTF8.GetBytes(messageRaw);
            var connectionKeys = _connections.Keys;
            foreach (var connectionKey in connectionKeys)
            {
                var arraySegment = new ArraySegment<byte>(buffer, 0, buffer.Length);
                await _connections[connectionKey].SendAsync(arraySegment, WebSocketMessageType.Text, true, CancellationToken.None);
            }
        }
    }
}
