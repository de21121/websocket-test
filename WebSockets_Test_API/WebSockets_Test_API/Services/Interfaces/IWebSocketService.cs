﻿using System.Net.WebSockets;

namespace WebSockets_Test_API.Services.Interfaces
{
    public interface IWebSocketService
    {
        Task RecieveMessage(WebSocket socket, Guid connectionId, string? userName = null);
    }
}
