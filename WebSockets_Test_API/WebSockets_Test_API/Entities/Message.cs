﻿namespace WebSockets_Test_API.Entities
{
    public class Message
    {
        public Guid Id { get; set; }
        public string User { get; set; }
        public string Body { get; set; }
        public DateTime Time { get; set; }
    }
}
