using WebSockets_Test_API.Services.Interfaces;
using WebSockets_Test_API.Services;
using Microsoft.AspNetCore.HttpOverrides;

try
{

    var builder = WebApplication.CreateBuilder(args);

    // Add services to the container.

    builder.Services.AddControllers();
    // Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
    builder.Services.AddEndpointsApiExplorer();
    builder.Services.AddSwaggerGen();
    builder.Services.AddSingleton<IWebSocketService>(new WebSocketService());

    var app = builder.Build();

    app.UseForwardedHeaders(new ForwardedHeadersOptions
    {
        ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto
    });

    // Configure the HTTP request pipeline.

    app.UseDeveloperExceptionPage();
    //app.UseSwagger();
    //app.UseSwaggerUI(c => { c.SwaggerEndpoint("/swagger/v1/swagger.json", "WebSocketsTest"); });
    app.UseCors("AllowAll");


    app.UseHttpsRedirection();

    app.UseAuthorization();
    app.UseWebSockets(new WebSocketOptions
    {
        KeepAliveInterval = TimeSpan.FromMinutes(200)
    });


    app.MapControllers();

    await app.RunAsync();
} catch (Exception e)
{
    throw;
}